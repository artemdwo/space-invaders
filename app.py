""" Space invaders: Ryan vs Aliens """
import os
import random
import pygame

pygame.font.init()

WIDTH, HEIGHT = 800, 800
WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Ryan vs. Space Invaders")

# Load images

# Aliens' spaceships and lasers
# RED
RED_SPACESHIP = pygame.image.load(
    os.path.join("assets", "pixel_ship_red_small.png"))
RED_LASER = pygame.image.load(os.path.join("assets", "pixel_laser_red.png"))
# GREEN
GREEN_SPACESHIP = pygame.image.load(
    os.path.join("assets", "pixel_ship_green_small.png"))
GREEN_LASER = pygame.image.load(
    os.path.join("assets", "pixel_laser_green.png"))
# BLUE
BLUE_SPACESHIP = pygame.image.load(
    os.path.join("assets", "pixel_ship_blue_small.png"))
BLUE_LASER = pygame.image.load(os.path.join("assets", "pixel_laser_blue.png"))

# Player's spaceship and laser
YELLOW_SPACESHIP = pygame.image.load(
    os.path.join("assets", "pixel_ship_yellow.png"))
YELLOW_LASER = pygame.image.load(
    os.path.join("assets", "pixel_laser_yellow.png"))

# Background
BG = pygame.transform.scale(pygame.image.load(
    os.path.join("assets", "background-black.png")), (WIDTH, HEIGHT))


class Ship:
    """ Base spaceship class """
    COOLDOWN = 30

    def __init__(self, x, y, health=100):
        # X-axis position
        self.x = x
        # Y-axis position
        self.y = y
        # Ship's health
        self.health = health
        # Ship's image
        self.ship_image = None
        # Ship's laser image
        self.laser_image = None
        # Ship's lasers
        self.lasers = []
        # Ship's cooldown times
        self.cool_down_counter = 0

    def draw(self, win):
        """ Draw a spaceship """
        win.blit(self.ship_image, (self.x, self.y))
        for laser in self.lasers:
            laser.draw(win)

    def move_lasers(self, vel, obj):
        """ Move lasers coming out of a spaceship """
        self.cooldown()
        for laser in self.lasers:
            laser.move(vel)
            if laser.off_screen(HEIGHT):
                self.lasers.remove(laser)
            elif laser.collision(obj):
                obj.health -= 10
                self.lasers.remove(laser)

    def get_width(self):
        """ Returns spaceship's width """
        return self.ship_image.get_width()

    def get_height(self):
        """ Returns spaceship's height """
        return self.ship_image.get_height()

    def shoot(self):
        """ Shoots spaceship's laser """
        if self.cool_down_counter == 0:
            laser = Laser(self.x, self.y, self.laser_image)
            self.lasers.append(laser)
            self.cool_down_counter = 1

    def cooldown(self):
        """ Laser's colldown period to control frequency """
        if self.cool_down_counter >= self.COOLDOWN:
            self.cool_down_counter = 0
        elif self.cool_down_counter > 0:
            self.cool_down_counter += 1

class Player(Ship):
    """ Player's spaceship """
    def __init__(self, x, y, health=100):
        super().__init__(x, y, health)
        self.ship_image = YELLOW_SPACESHIP
        self.laser_image = YELLOW_LASER
        self.mask = pygame.mask.from_surface(self.ship_image)
        self.max_health = health

    def move_lasers(self, vel, objs):
        """ Move player's lasers """
        self.cooldown()
        for laser in self.lasers:
            laser.move(vel)
            if laser.off_screen(HEIGHT):
                self.lasers.remove(laser)
            else:
                for obj in objs:
                    if laser.collision(obj):
                        objs.remove(obj)
                        if laser in self.lasers:
                            self.lasers.remove(laser)

    def draw(self, win):
        """ Draw spaceship's health bar """
        super().draw(win)
        self.health_bar(win)

    def health_bar(self, win):
        """ Define/redefine the health bar """
        pygame.draw.rect(win, (255, 0, 0), (self.x, self.y +
                         self.ship_image.get_height() + 10, self.ship_image.get_width(), 10))
        pygame.draw.rect(win, (0, 255, 0), (self.x, self.y + self.ship_image.get_height() +
                         10, self.ship_image.get_width() * (self.health/self.max_health), 10))


class Alien(Ship):
    """ Aliens' ships """

    SHIP_COLOR_MAP = {
        "red": (RED_SPACESHIP, RED_LASER),
        "green": (GREEN_SPACESHIP, GREEN_LASER),
        "blue": (BLUE_SPACESHIP, BLUE_LASER)
    }

    def __init__(self, x, y, color, health=100):
        super().__init__(x, y, health)
        self.ship_image, self.laser_image = self.SHIP_COLOR_MAP[color]
        self.mask = pygame.mask.from_surface(self.ship_image)

    def move(self, vel):
        """ Move an alien ship """
        self.y += vel

    def shoot(self):
        """ Control alien's shooting at the player """
        if self.cool_down_counter == 0:
            laser = Laser(self.x - self.ship_image.get_width() /
                          3, self.y, self.laser_image)
            self.lasers.append(laser)
            self.cool_down_counter = 1


class Laser(object):
    """ Laser class """
    def __init__(self, x, y, img):
        self.x = x
        self.y = y
        self.img = img
        self.mask = pygame.mask.from_surface(self.img)

    def draw(self, win):
        """ Draw a laser """
        win.blit(self.img, (self.x, self.y))

    def move(self, vel):
        """ Move a laser """
        self.y += vel

    def off_screen(self, height):
        """ Return wether laser is off the screen """
        return not(self.y <= height and self.y >= 0)

    def collision(self, obj):
        """ Return wether there has been any collision """
        return collide(obj, self)


def collide(obj_1, obj_2):
    """ Checks and returns a collision """
    offset_x = obj_2.x - obj_1.x
    offset_y = obj_2.y - obj_1.y
    return obj_1.mask.overlap(obj_2.mask, (offset_x, offset_y)) is not None


def main():
    """ Main game method """
    run = True
    FPS = 60
    main_font = pygame.font.SysFont('comicsans', 50)
    lost_font = pygame.font.SysFont('comicsans', 60)

    player = Player(300, 685)

    # Variable game's attributes
    # Game's level
    level = 0
    # Player's lives count
    lives = 5
    # Movement velocity
    velocity = 5
    # Aliens' wave length
    aliens = []
    aliens_wave_length = 5
    aliens_velocity = 1
    # Lasers' movement velocity
    laser_velocity = 8

    lost = False
    lost_timer = 0

    clock = pygame.time.Clock()

    def redraw_window():
        WINDOW.blit(BG, (0, 0))

        # Draw text
        lives_label = main_font.render(f"Lives: {lives}", 1, (255, 255, 255))
        WINDOW.blit(lives_label, (WIDTH - lives_label.get_width() - 10, 10))
        level_label = main_font.render(f"Level: {level}", 1, (255, 255, 255))
        WINDOW.blit(level_label, (10, 10))

        for alien in aliens:
            alien.draw(WINDOW)

        player.draw(WINDOW)

        if lost:
            lost_label = lost_font.render(
                "You lost this time!", 1, (255, 255, 255))
            WINDOW.blit(lost_label, (WIDTH/2 - lost_label.get_width()/2, 350))

        pygame.display.update()

    while run:
        clock.tick(FPS)
        redraw_window()

        if lives <= 0 or player.health <= 0:
            lost = True
            lost_timer += 1

        if lost:
            if lost_timer > FPS * 5:
                run = False
            else:
                continue

        if len(aliens) == 0:
            level += 1
            aliens_wave_length += 5
            for i in range(aliens_wave_length):
                alien = Alien(
                    random.randrange(50, WIDTH - 100),
                    random.randrange(-1500, -100),
                    random.choice(["red", "green", "blue"])
                    )
                aliens.append(alien)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                quit()

        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and player.x - velocity > 0:
            player.x -= velocity
        if keys[pygame.K_RIGHT] and player.x + velocity + player.get_width() < WIDTH:
            player.x += velocity
        if keys[pygame.K_UP] and player.y - velocity > 0:
            player.y -= velocity
        if keys[pygame.K_DOWN] and player.y + velocity + player.get_height() + 15 < HEIGHT:
            player.y += velocity
        if keys[pygame.K_SPACE]:
            player.shoot()

        for an_alien in aliens:
            an_alien.move(aliens_velocity)
            an_alien.move_lasers(laser_velocity, player)

            if random.randrange(0, 4 * FPS) == 1:
                an_alien.shoot()

            if collide(an_alien, player):
                player.health -= 10
                aliens.remove(an_alien)

            if an_alien.y + an_alien.get_height() > HEIGHT:
                lives -= 1
                aliens.remove(an_alien)

        player.move_lasers(-laser_velocity, aliens)

def main_menu():
    """ Handles main menu """
    title_font = pygame.font.SysFont('comicsans', 60)
    subtitle_font = pygame.font.SysFont('comicsans', 30)
    run = True
    while run:
        WINDOW.blit(BG, (0, 0))
        title_label = title_font.render("Press X to start the game!", 1, (255,255,255))
        WINDOW.blit(title_label, (WIDTH/2 - title_label.get_width()/2, 350))
        subtitle_label = subtitle_font.render("Trackpad or mouse button will do the same....", 1, (255,255,255))
        WINDOW.blit(subtitle_label, (WIDTH/2 - subtitle_label.get_width()/2, 350 + subtitle_label.get_height()*2))
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                main()
        keys = pygame.key.get_pressed()
        if keys[pygame.K_x]:
            main()
    pygame.quit()

if __name__ == '__main__':
    main_menu()
