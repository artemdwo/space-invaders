# Space Invaders
## Description

TBD

##  Development

## Python

Version: 3.10.1

## Dependencies

`./requirements.txt` 

To install all required dependencies:

`$ pip install -r requirements.txt`
### Structure

`./assets/` - spaceship graphic assets

`./app.py` - main game's file
